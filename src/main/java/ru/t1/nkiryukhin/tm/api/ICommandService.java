package ru.t1.nkiryukhin.tm.api;

import ru.t1.nkiryukhin.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
