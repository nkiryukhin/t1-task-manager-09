package ru.t1.nkiryukhin.tm.repository;

import ru.t1.nkiryukhin.tm.api.ICommandRepository;
import ru.t1.nkiryukhin.tm.constant.ArgumentConst;
import ru.t1.nkiryukhin.tm.constant.CommandConst;
import ru.t1.nkiryukhin.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info"
    );

    private static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO,
            "Show system info"
    );

    private static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP,
            "Show command list"
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT, null,
            "Close application"
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION,
            "Show application version"
    );

    private static final Command[] COMMANDS = new Command[] {
            ABOUT, INFO, HELP, VERSION, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
